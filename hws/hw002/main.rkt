#lang racket
(require eopl)
(require rackunit)
(require racket/match)
(provide (all-defined-out))


(define expressible-value? (or/c number? boolean?))
(struct exn:parse-error exn:fail ())

(define binop?
  (lambda (x)
    (match x
      ['add #t]
      ['sub #t]
      ['mul #t]
      ['div #t]
      ['lt? #t]
      ['eq? #t]
      [_ #f])))


(define-datatype ast ast?
 [binop (op binop?) (rand1 ast?) (rand2 ast?)]
 [ifte (c ast?) (t ast?) (e ast?)]
 [num (n number?)]
 [bool (b boolean?)])


(define (map op)
  (cond
    [(eq? op '+) 'add]
    [(eq? op '-) 'sub]
    [(eq? op '*) 'mul]
    [(eq? op '/) 'div]
    [(eq? op '<) 'lt?]
    [(eq? op '==) 'eq?]
    [else #f]))

(define raise-parse-error 
 (lambda (err-msg)
   (raise (exn:parse-error err-msg (current-continuation-marks)))))

(define (parse exp)
  (cond
    [(number? exp) (num exp)]
    [(boolean? exp) (bool exp)]
    [else (let ((mapped_op (map (car exp))) (l (car (cdr exp))) (r (car (cdr (cdr exp)))))
            (cond
              [mapped_op (binop mapped_op (parse l) (parse r))]
              [(eq? (car exp) 'if) (let ((w (car (cdr (cdr (cdr exp))))))
                                     (ifte (parse l) (parse r) (parse w)))]
              [else (raise-parse-error "invalid symbol")]))])
  )
(struct exn:exec-div-by-zero exn:fail ())
(define raise-exec-div-by-zero
  (lambda ()
    (raise (exn:exec-div-by-zero "div-by-0!" (current-continuation-marks)))))

(struct exn:exec-type-mismatch exn:fail ())
(define raise-exec-type-mismatch
  (lambda ()
    (raise (exn:exec-type-mismatch "type mismatch!" (current-continuation-marks)))))

(define op-interpretation
  (lambda (op)
    (match op
      ['add +]
      ['sub -]
      ['mul *]
      ['lt? <]
      ['eq? =]
      [_ error 'op-interpretation "unknown op"])))

(define eval-ast
  (lambda (a)
    (cases ast a
      (num (n) n)
      (bool (b) b)
      (binop (op l r) (let ((l_eval (eval-ast l)) (r_eval (eval-ast r)))
                        (cond
                          [(and (number? l_eval) (number? r_eval)) (cond
                                                           [(eq? 'div op) (if (zero? r_eval) (raise-exec-div-by-zero) (/ l_eval r_eval))]
                                                           [else ((op-interpretation op) l_eval r_eval)])]
                          [else (raise-exec-type-mismatch)])))
      (ifte (c t e) (let ((truth-val (eval-ast c)))
                      (if (boolean? truth-val) (if truth-val (eval-ast t) (eval-ast e)) (raise-exec-type-mismatch)))))))

;;; runtime-check :: [expressible? -> boolean?], exn? -> [expressible? -> expressible? || exn?] 
(define runtime-check
  (lambda (pred? exn)
    (lambda (v)
      (if (pred? v)
          v
          (exn)))))

(define typecheck-num
  (runtime-check number?  raise-exec-type-mismatch))

(define typecheck-bool 
  (runtime-check boolean? raise-exec-type-mismatch))

(define check-non-zero
  (runtime-check (not/c zero?) raise-exec-div-by-zero))


(define ts-numop-incorrect-param-rand1
  (test-suite 
   "wrongly typed rand1 parameters"
   (for/list ([numerical-op '(add sub mul div lt? eq?)])
     (test-case (string-append (symbol->string numerical-op) "-type-mismatch-rand1")
       (check-exn exn:exec-type-mismatch?
                  (lambda () 
                    (eval-ast (binop numerical-op
                                     (binop 'lt? (num 10) (num 20)) ; boolean
                                     (num 10)))))))))

(define ts-numop-incorrect-param-rand2
  (test-suite
   "wrongly typed rand2 parameters"
   (for/list ([numerical-op '(add sub mul div)])
     (test-case (string-append (symbol->string numerical-op) "-type-mismatch-rand1")
       (check-exn exn:exec-type-mismatch?
                  (lambda () 
                    (eval-ast (binop numerical-op (num 10)
                                     (binop 'lt? (num 10) (num 20))))))))))


