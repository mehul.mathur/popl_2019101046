#+NAME: Homework Assignment 01 : Solution

* Introduction

  This is the solution to the sample homework assigment.

* Problem No. 1.1 : Repeat x n times

** Solution
  The function =repeat n x= needs to be performed recursively, where the changing element is n.
  1. Base case - If n is 0 , return empty list
  2. Normal case - Return a constructed list with first element as x and next elements as the return value of =repeat n-1 x=

#+NAME: repeat
#+BEGIN_SRC scheme
  (define (repeat n x)
    (cond
      [(> n 0) (cons x (repeat (- n 1) x))]
      [else '()]))
#+END_SRC

* Problem No. 1.2 : Invert internal 2-lists in a list

** Solution

#+NAME: invert
#+BEGIN_SRC scheme
  (define (invert lst)
  (cond
    [(null? lst) '()]
    [else (cons (cons (car (cdr (car lst))) (cons (car (car lst)) '()))  (invert (cdr lst)))]))
#+END_SRC

* Problem No. 1.3 : Count occurences of an element in a list

** Solution

#+NAME: count-occurences
#+BEGIN_SRC scheme
(define (count-occurrences s slist)
  (cond
    [(null? slist) 0]
    [(equal? s (car slist)) (+ 1 (count-occurrences s (cdr slist)))]
    [else (count-occurrences s (cdr slist))]))
#+END_SRC

* Problem No. 1.4 : Cartesian product of 2 lists

** Solution

#+NAME: count-occurences
#+BEGIN_SRC scheme
(define (single_product x sos)
  (cond
    [(null? sos) '()]
    [else (cons (cons x (cons (car sos) '())) (single_product x (cdr sos)))]))


(define (product sos1 sos2)
  (cond
    [(null? sos1) sos2]
    [(null? sos2) sos1]
    [else (cond
            [(null? (cdr sos1)) (single_product (car sos1) sos2)]
            [else (append (single_product (car sos1) sos2) (product (cdr sos1) sos2))])]))
#+END_SRC

* Problem No. 1.5 : A function called 'every' to test all elements of a list against a boolean test

** Solution

#+NAME: every
#+BEGIN_SRC scheme
(define (every pred lst)
  (cond
    [(null? lst) #t]
    [else (and (pred (car lst)) (every pred (cdr lst)))]))
#+END_SRC

* Problem No. 1.6 : Merge 2 sorted arrays

** Solution

#+NAME: merge
#+BEGIN_SRC scheme
(define (merge loi1 loi2)
  (cond
    [(null? loi1) loi2]
    [(null? loi2) loi1]
    [else (cond
            [(> (car loi1) (car loi2)) (append (list (car loi2)) (merge loi1 (cdr loi2)))]
            [else (append (list (car loi1)) (merge (cdr loi1) loi2))])]))
#+END_SRC

* Problem No. 1.7 : Flatten a nested list

** Solution

#+NAME: flatten
#+BEGIN_SRC scheme
(define (flatten dlst)
  (cond
    [(null? dlst) '()]
    [else (cond
            [(list? (car dlst)) (append (flatten (car dlst)) (flatten (cdr dlst)))]
            [else (append (list (car dlst)) (flatten (cdr dlst)))])]))
#+END_SRC

* Problem No. 2.1 : Preorder tree traversal

** Solution

#+NAME: traverse/preorder
#+BEGIN_SRC scheme
(define (traverse/preorder tree)
	(cases full-binary-tree tree
		(leaf-node (v) (list v))
		(internal-node (v lc rc) (append (list v) (traverse/preorder lc) (traverse/preorder rc)))))
#+END_SRC

* Problem No. 2.2 : Inorder tree traversal

** Solution

#+NAME: traverse/inorder
#+BEGIN_SRC scheme
(define (traverse/inorder tree)
	(cases full-binary-tree tree
		(leaf-node (v) (list v))
		(internal-node (v lc rc) (append (traverse/inorder lc) (list v) (traverse/inorder rc)))))
#+END_SRC

* Problem No. 2.3 : Postorder tree traversal

** Solution

#+NAME: traverse/postorder
#+BEGIN_SRC scheme
(define (traverse/postorder tree)
	(cases full-binary-tree tree
		(leaf-node (v) (list v))
		(internal-node (v lc rc) (append (traverse/postorder lc) (traverse/postorder rc) (list v)))))
#+END_SRC

* Problem No. 2.4 : Count all nodes of a tree

** Solution

#+NAME: count-nodes
#+BEGIN_SRC scheme
(define (count-nodes tree)
  (cases full-binary-tree tree
    (leaf-node (v) 1)
    (internal-node (v lc rc) (+ 1 (+ (count-nodes lc) (count-nodes rc))))))
#+END_SRC

* Problem No. 2.5 : Count all leaves of a tree

** Solution

#+NAME: count-leaves
#+BEGIN_SRC scheme
(define (count-leaves tree)
  (cases full-binary-tree tree
    (leaf-node (v) 1)
    (internal-node (v lc rc) (+ (count-leaves lc) (count-leaves rc)))))
#+END_SRC

* Problem No. 2.6 : Count all internal nodes of a tree

** Solution

#+NAME: count-internal
#+BEGIN_SRC scheme
(define (count-internal tree)
  (cases full-binary-tree tree
    (leaf-node (v) 0)
    (internal-node (v lc rc) (+ 1 (+ (count-internal lc) (count-internal rc))))))
#+END_SRC


* Problem No. 2.7 : Map a function to all nodes of a tree

** Solution

#+NAME: tree/map
#+BEGIN_SRC scheme
(define (tree/map fn tr)
  (cases full-binary-tree tr
    (leaf-node (v) (lnode (fn v)))
    (internal-node (v lc rc) (inode (fn v) (tree/map fn lc) (tree/map fn rc)))))
#+END_SRC

* Problem No. 2.8 : Value at a node on a particular path

** Solution

#+NAME: value-at-path
#+BEGIN_SRC scheme
(define (value-at-path path tree)
  (cases full-binary-tree tree
    (leaf-node (v) (cond
                     [(null? path) v]
                     [else "Invalid Path"]))
    (internal-node (v lc rc) (cond
                               [(null? path) v]
                               [(eq? (car path) "left") (value-at-path (cdr path) lc)]
                               [else (value-at-path (cdr path) rc)]))))
#+END_SRC

* Problem No. 2.9 : Search a value in a tree and return path

** Solution

#+NAME: search
#+BEGIN_SRC scheme
(define (search_helper val tree)
  (cases full-binary-tree tree
    (leaf-node (v) (cond
                     [(= val v) (cons #t '())]
                     [else (cons #f '())]))
    (internal-node (v lc rc) (cond
                               [(= val v) (cons #t '())]
                               [else (let ((l_ret (search_helper val lc)) (r_ret (search_helper val rc)))
                                       (cond
                                         [(car l_ret) (cons #t (cons "left" (cdr l_ret)))]
                                         [(car r_ret) (cons #t (cons "right" (cdr r_ret)))]
                                         [else (cons #f '())]))]))))

(define (search val tree)
  (let ((ret (search_helper val tree)))
    (cond
      [(car ret) (cdr ret)]
      [else "Does not exist"])))
#+END_SRC

* Problem No. 2.10 : Update a node at a path

** Solution

#+NAME: update
#+BEGIN_SRC scheme
(define (update path fn tr)
  (cases full-binary-tree tr
    (leaf-node (v) (cond
                     [(null? path) (lnode (fn v))]
                     [else (error "Invalid path")]))
    (internal-node (v lc rc) (cond
                               [(null? path) (inode (fn v) lc rc)]
                               [(eq? "left" (car path)) (inode v (update (cdr path) fn lc) rc)]
                               [else (inode v lc (update (cdr path) fn rc))]))))
#+END_SRC

* Problem No. 2.11 : Insert two children at a leaf node

** Solution

#+NAME: tree/insert
#+BEGIN_SRC scheme
(define (tree/insert path left-st right-st tree)
  (cases full-binary-tree tree
    (leaf-node (v) (cond
                     [(null? path) (inode v left-st right-st)]
                     [else (error "Invalid path")]))
    (internal-node (v lc rc) (cond
                               [(null? path) (error "internal node")]
                               [(eq? "left" (car path)) (inode v (tree/insert (cdr path) left-st right-st lc) rc)]
                               [else (inode v lc (tree/insert (cdr path) left-st right-st rc))]))))
#+END_SRC








